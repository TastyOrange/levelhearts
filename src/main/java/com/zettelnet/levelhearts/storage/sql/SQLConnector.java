package com.zettelnet.levelhearts.storage.sql;

import java.sql.Connection;
import java.sql.SQLException;

public interface SQLConnector {

	Connection createConnection() throws SQLException;
}
