package com.zettelnet.levelhearts.storage;

import org.bukkit.entity.Player;

public interface HealthStorage {

	public boolean initialize();

	public boolean terminate();

	/**
	 * @return whether the player has played before
	 */
	public boolean loadPlayer(Player player);

	public void unloadPlayer(Player player);

	public boolean isPlayerLoaded(Player player);

	public boolean isRespawn();

	public double getHealth(Player player);

	public boolean setHealth(Player player, double health);

	public double getMaxHealth(Player player);

	public boolean setMaxHealth(Player player, double maxHealth);

	@Deprecated
	public static enum Mode {
		PHYSICAL, MYSQL, SQLITE;
	}
}
