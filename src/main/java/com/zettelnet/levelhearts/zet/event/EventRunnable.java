package com.zettelnet.levelhearts.zet.event;

import org.bukkit.event.Event;

public abstract class EventRunnable<T extends Event> {

	public EventRunnable() {
	}

	public abstract void run(T event);
}
