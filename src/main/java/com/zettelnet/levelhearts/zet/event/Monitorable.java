package com.zettelnet.levelhearts.zet.event;

import java.util.Set;
import org.bukkit.event.Event;

public interface Monitorable<T extends Event> {

	public void addMonitor(EventRunnable<T> runnable);

	public Set<EventRunnable<T>> getMonitors();
}
